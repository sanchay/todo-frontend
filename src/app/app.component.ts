import { Component } from '@angular/core';

import { Todo } from "./todos/todo";
import { TodoDataService } from "./todos/todo-data.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TodoDataService]
})
export class AppComponent {
  title = 'ToDo';

  dropdown: boolean = false;
  newTodo: Todo = new Todo();

  constructor(private todoDataService: TodoDataService) {}

  toggleDropdown() {
    this.dropdown = !this.dropdown;
    this.newTodo.parent = null;
  }

  addTodo() {
    this.todoDataService.addTodo(this.newTodo);
    //this.newTodo.title = 'moba';
    this.newTodo = new Todo();
    console.log(this.todos);
  }

  toggleTodoComplete(todo) {
    this.todoDataService.toggleTodoComplete(todo);
  }

  removeTodo(todo) {
    this.todoDataService.deleteTodoById(todo.id);
  }

  get todos() {
    console.log(this.todos);
    return this.todoDataService.getAllTodos();
  }

  get tasks() {
    return this.todoDataService.getParentTasks();
  }

  addInitialTodo(todo) {
    this.todoDataService.addTodo(todo);
  }

  ngOnInit() {
    /*let initTodo = new Todo();
    initTodo = {'title' : 'Task1', 'due_date' : null, 'complete': false, 'id' : null , parent: 3 };
    this.addInitialTodo(initTodo);
    initTodo = {'title' : 'Task2', 'due_date' : null, 'complete': true, 'id' : null , parent: 3 };
    this.addInitialTodo(initTodo);
    initTodo = {'title' : 'Task3', 'due_date' : null, 'complete': true, 'id' : null , parent: null };
    this.addInitialTodo(initTodo);*/
    console.log(this.todos);
  }

  
}
